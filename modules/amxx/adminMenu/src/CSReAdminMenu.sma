#include <amxmodx>
#include <amxmisc>
#include <csrankre>

#define PLUGIN "CSRE: AdminMenu"
#define VERSION "1.0.0"
#define AUTHOR "Valter(https://vk.com/id524101682)"

const MAXPLAYERS = 32
const SKIN = 4

static CSR_VALUE_TYPE[5] = 
{
	CSRE_VALUE_COINS,
	CSRE_VALUE_CASES,
	CSRE_VALUE_KEYS, 
	CSRE_VALUE_GOLDS,
	SKIN
}

static const CHAT_RESULT[][32] = 
{
	"CHAT_RESULT_COIN",
	"CHAT_RESULT_CONTAINER",
	"CHAT_RESULT_KEY",
	"CHAT_RESULT_GOLD",
	"CHAT_RESULT_SKIN"
}

enum _:aID 
{
	SteamID[22]
}

new g_AdminMenu
new g_SubMenuPlayers
new g_SubMenuGiveItem
new g_SelectedPIndex[33]
new g_Player[MAXPLAYERS + 1][aID]
new g_PlayerType[33]

public plugin_init()
{	
	register_plugin(PLUGIN, VERSION, AUTHOR);
	register_concmd("csre_admin_menu", "CSREAdminMenu")
	
	register_clcmd("CSREValue", "HandleInput")
	register_dictionary("CSRankReAdminMenu.txt")
	register_dictionary("CSRankRe.txt")
}

public client_connect(id) 
{
	g_PlayerType[id] = -1
	g_SelectedPIndex[id] = 0
}

public CSREAdminMenu(id)
{	
	if (!Has_access(id)) 
		return client_print(id, print_chat, "You don't have access to this menu")

	new title[128]; formatex(title, charsmax(title), "%L", id, "CSRE_ADMIN_MENU")
	
	g_AdminMenu = menu_create(title, "HandleAdminMenu");
	
	MenuSetProps(id, g_AdminMenu)

	formatex(title, charsmax(title), "%L", id, "CSRE_ADMIN_MENU_ADD")
	menu_additem(g_AdminMenu, title, "1", 0)
	
	return menu_display(id, g_AdminMenu, 0)
}

public HandleAdminMenu(id, menu, item)
{
	new s_Data[6], s_Name[1], i_Access, i_Callback
	menu_item_getinfo(menu, item, i_Access, s_Data, charsmax(s_Data), s_Name, charsmax(s_Name), i_Callback)

	new i_Key = str_to_num(s_Data)

	switch(i_Key)	
		{
		case 1:	
		{
			SubMenuPlayers(id)
		}
		}
	return PLUGIN_HANDLED
}

public SubMenuPlayers(id)
{	
	new szText[128]
	
	formatex(szText, charsmax(szText), "%L", id, "TITLE_PLAYERS_MENU")
	g_SubMenuPlayers = menu_create(szText, "SubMenuPlayers_handler")
	
	MenuSetProps(id, g_SubMenuPlayers)
	
	new s_Players[32], i_Num, i_Player
	new s_Name[32], s_Player[10] 

	get_players(s_Players, i_Num, "h")
	
	for (new i; i < i_Num; i++)
	{
		i_Player = s_Players[i]
		get_user_name(i_Player, s_Name, charsmax(s_Name))
		num_to_str(i_Player, s_Player, charsmax(s_Player))

		get_user_authid(i_Player, g_Player[i_Player][SteamID], 22)
		
		menu_additem(g_SubMenuPlayers, s_Name, s_Player, 0)
	}
	
	menu_display(id, g_SubMenuPlayers, 0)
}

public SubMenuPlayers_handler(id, menu, item)
 {
	if (item == MENU_EXIT)	
	{
		return is_user_connected(id) ? menu_display(id, g_AdminMenu, 0) : PLUGIN_HANDLED
	}
	
	new s_Data[6], s_Name[1], i_Access, i_Callback;
	menu_item_getinfo(menu, item, i_Access, s_Data, charsmax(s_Data), s_Name, charsmax(s_Name), i_Callback)
	new i_Player = str_to_num(s_Data)
	
	g_SelectedPIndex[id] = i_Player
	
	SubMenuGiveItem(id)
	
	return PLUGIN_HANDLED
} 

public SubMenuGiveItem(id) 
{
	new szText[128]
	
	formatex(szText, charsmax(szText), "%L", id, "TITLE_GIVE_ITEM_MENU")
	g_SubMenuGiveItem = menu_create(szText, "SubMenuGiveItem_handler")
	MenuSetProps(id, g_SubMenuGiveItem)
	
	static s_itemId[10]
	static menu_item[32] = "GIVE_MENU_ITEM_ID_" 
	
	for (new i = 1; i <= sizeof(CSR_VALUE_TYPE); i++)
	{
		num_to_str(i, s_itemId, charsmax(s_itemId))
		add(menu_item,charsmax(menu_item),s_itemId)
		
		formatex(szText, charsmax(szText), "%L", id, menu_item)
		menu_additem(g_SubMenuGiveItem, szText, s_itemId)
		
		replace(menu_item, charsmax(menu_item), s_itemId, "");
	}

	menu_display(id, g_SubMenuGiveItem, 0)
}

public SubMenuGiveItem_handler(id, menu, item) 
{
	if (item == MENU_EXIT)	
		return is_user_connected(id) ? menu_display(id, g_SubMenuPlayers, 0) : PLUGIN_HANDLED

	new s_Data[6], s_Name[1], i_Access, i_Callback
	menu_item_getinfo(menu, item, i_Access, s_Data, charsmax(s_Data), s_Name, charsmax(s_Name), i_Callback)

	new i_Key = str_to_num(s_Data)
	
	g_PlayerType[id] = i_Key - 1
	
	if (g_PlayerType[id] < 4) 
	{
		EnterValue(id)
	}
	else
	{
		SubMenuSkinCatalog(id)
	}
	
	return PLUGIN_HANDLED
}

public SubMenuSkinCatalog(id)
{
	new title[128]; formatex(title, charsmax(title), "%L", id, "TITLE_SKIN_MENU")
	
	new Menu = menu_create(title, "SubMenuSkinCatalog_handler");

	MenuSetProps(id, Menu)

	new Array:skins = csre_get_all_skins()
	new size = ArraySize(skins), data[nSkins]
	new s_SkinName[64], skinID[12]
	
	for(new i; i < size; i++) 
	{
		ArrayGetArray(skins, i, data)
		MenuFormatSkin(data, s_SkinName, charsmax(s_SkinName))
		num_to_str(data[SKIN_N_ID], skinID, charsmax(skinID))
		menu_additem(Menu, s_SkinName, skinID)
	}
	
	ArrayDestroy(skins)
	
	menu_display(id, Menu, 0)
}

public SubMenuSkinCatalog_handler(id, menu, item) 
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return is_user_connected(id) ? menu_display(id, g_SubMenuGiveItem, 0) : PLUGIN_HANDLED
	}
		
	new s_Data[6], s_skinName[32], i_Access, i_Callback, skinID
	menu_item_getinfo(menu, item, i_Access, s_Data, charsmax(s_Data), s_skinName, charsmax(s_skinName), i_Callback)
	
	skinID = str_to_num(s_Data)

	Extradition_handler(id, skinID) 

	return PLUGIN_HANDLED
}

public EnterValue(id) 
{
	client_cmd(id, "messagemode CSREValue")
}

public HandleInput(id)
{
	if(!Has_access(id))
		return 1
	
	new inputText[28]
	
	read_args(inputText, charsmax(inputText))
	remove_quotes(inputText)

	if(!IsStrInt(inputText))
		return client_print(id, print_chat, "%L", id, "ERR_VAL_NOT_NUM")
	
	new value = str_to_num(inputText)
	
	Extradition_handler(id, value)
	
	return PLUGIN_HANDLED
}

public Extradition_handler(id, value) 
{
	if (!ValidatePlayerToPass(id)) 
		return client_print(id, print_chat, "%L", id, "ERR_IDENT")
	
	new s_Name[32]
	
	if (g_PlayerType[id] == 4)
	{	
		new skinID = value, info[75]
		
		get_user_name(g_SelectedPIndex[id], s_Name, charsmax(s_Name))
		SkinInfo(skinID, info, charsmax(info))
		
		csre_add_skin(g_SelectedPIndex[id], skinID)
		client_print(id, print_chat, "%L", id, CHAT_RESULT[g_PlayerType[id]], s_Name, info)
		
		return 1
	}
	
	get_user_name(g_SelectedPIndex[id], s_Name, charsmax(s_Name))
	
	if (csre_get_value(g_SelectedPIndex[id], CSR_VALUE_TYPE[g_PlayerType[id]]) + value < 0 ) 
		return client_print(id, print_chat,"Значение недопустимо , предмету присваивается количество ниже 0")
	
	csre_add_value(g_SelectedPIndex[id], CSR_VALUE_TYPE[g_PlayerType[id]], value)
	client_print(id, print_chat,"%L", id, CHAT_RESULT[g_PlayerType[id]], value, s_Name)
	
	return PLUGIN_HANDLED
}

stock MenuSetProps(id, menu)
{
	static const plugin_x_format[] = "\wCSRank\rRe"

	new szText[64]
	formatex(szText, sizeof(szText) - 1, "%L", id, "CSRE_MENU_PREV")
	menu_setprop(menu, MPROP_BACKNAME, szText)
	
	formatex(szText, sizeof(szText) - 1, "%L", id, "CSRE_MENU_NEXT")
	menu_setprop(menu, MPROP_NEXTNAME, szText)
	
	formatex(szText, sizeof(szText) - 1, "%L^n^n%s", id, "CSRE_MENU_EXIT", plugin_x_format)
	menu_setprop(menu, MPROP_EXITNAME, szText)
	
	return 1
} 

bool:ValidatePlayerToPass(id)
{
	new fresh_SteamID[22];
	get_user_authid(g_SelectedPIndex[id], fresh_SteamID, 22)
	
	if (equali(g_Player[g_SelectedPIndex[id]][SteamID], fresh_SteamID))	
		return true

	return false
}
bool:IsStrInt(string[])
{
	new len = strlen(string);
	for ( new i = 0; i < len; i++ )
	{
	switch ( string[i] )
	{
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-': continue;
		default: return false;
	}
	}
	return true;
}

bool:Has_access(id)
{
	new s_Flag[26]
	get_cvar_string("csre_superadmin_flag", s_Flag, 26);
	
	if(has_flag(id, s_Flag))
		return true

	return false
}

stock MenuFormatSkin(data[nSkins], str[], len) 
{
	new itemColor[] = "\w";
	switch (data[SKIN_N_LEVEL]) 
	{
		case 3: formatex(itemColor, charsmax(itemColor), "\y");
		case 2: formatex(itemColor, charsmax(itemColor), "\r");
		case 1: formatex(itemColor, charsmax(itemColor), "\w");
	}
	return formatex(str, len, "%s%s", itemColor, data[SKIN_N_NAME])
}

stock SkinInfo(skinID, info[], len) 
{
	new Array:skins = csre_get_all_skins()
	new _info[64], data[nSkins]
	
	for(new i; i < ArraySize(skins); i++) 
	{
		ArrayGetArray(skins, i, data)
		if (data[SKIN_N_ID] == skinID)
		{
			formatex(_info, charsmax(_info), "%s [class %d]", data[SKIN_N_NAME], data[SKIN_N_LEVEL])
			break
		}
	}
	ArrayDestroy(skins)
	return formatex(info, len, "%s", _info )
}